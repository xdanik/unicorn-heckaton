const AppClient = require("uu_appg01_server").AppClient;
const { Validator } = require("uu_appg01_server").Validation;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const { UseCaseError } = require("uu_appg01_server").AppServer;

const { dtoIn, console, session } = scriptContext;

/*@@viewOn:dtoIn*/
const dtoInSchema = `const sdeExamTask01DtoInSchemaType = shape({
  count: integer(2000).isRequired(),
  age: shape({
    min: integer(0, 100).isRequired(),
    max: integer(0, 100).isRequired()
  })
})`;
/*@@viewOff:dtoIn*/

/*@@viewOn:errors*/
const Errors = {
  ERROR_PREFIX: "ucl/sdeExamTask01/",

  InvalidDtoIn: class extends UseCaseError {
    constructor(dtoOut, paramMap) {
      super({ dtoOut, paramMap, status: 400 });
      this.message = `DtoIn is not valid.`;
      this.code = `${Errors.ERROR_PREFIX}invalidDtoIn`;
    }
  }
};
/*@@viewOff:errors*/

/*@@viewOn:helpers*/
function _getRandom(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function _validateDtoIn(dtoIn, uuAppErrorMap) {
  //2.1.1
  let dtoInValidator = new Validator(dtoInSchema, true);
  //2.1.2
  let validationResult = dtoInValidator.validate("sdeExamTask01DtoInSchemaType", dtoIn);
  uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult, `${Errors.ERROR_PREFIX}unsupportedKeys`, Errors.InvalidDtoIn);
  //2.1.3
  return uuAppErrorMap;
}

function _getCard(name, surname, birthdate, gender) {
  return `<UU5.Bricks.Card elevation=1 className="uu5-common-padding-m">
    <UU5.Bricks.Block colorSchema="${gender === "male" ? "blue" : "pink"}">
      <UU5.Bricks.Header style="margin-top: 0" level=2>
        <UU5.Bricks.Icon icon="${gender === "male" ? "mdi-gender-male" : "mdi-gender-female"}"/> ${name} ${surname} 
      </UU5.Bricks.Header>
      <UU5.Bricks.DateTime dateOnly value="${birthdate}" />
    </UU5.Bricks.Block>
  </UU5.Bricks.Card>`;
}

function _getData(data) {
  return `<UuApp.DesignKit.EmbeddedText codeStyle=javascript showLineNumbers=false codeStyle="javascript">${JSON.stringify(data)}</UuApp.DesignKit.EmbeddedText>`;
}
/*@@viewOff:helpers*/

async function main() {
  //1
  let uuAppErrorMap = {};

  //2
  await _validateDtoIn(dtoIn, uuAppErrorMap);

  /*@@viewOn:sourceCode*/
  

  // https://www.czso.cz/csu/xh/nejcastejsi-jmena-a-prijmeni-obyvatel-kraje-v-roce-2016
  const MALE = 'male',
    FEMALE = 'female',
    maleSurnames = [
    'Novák',
    'Novotný',
    'Černý',
    'Horák',
    'Svoboda',
    'Dvořák',
    'Marek',
    'Doležal',
    'Hanuš',
    'Kučera',
    'Bartoš',
    'Kašpar',
    'Kopecký',
    'Dostál',
    'Sedláček',
    'Beneš',
    'Mach',
    'Malý',
    'Šimek',
    'Nosek',
  ], femaleSurnames = [
    'Nováková',
    'Novotná',
    'Černá',
    'Svobodová',
    'Horáková',
    'Dvořáková',
    'Marková',
    'Doležalová',
    'Hanušová',
    'Kučerová',
    'Kopecká',
    'Bartošová',
    'Sedláčková',
    'Machová',
    'Kašparová',
    'Dostálová',
    'Šimková',
    'Nosková',
    'Dušková',
    'Benešová',
  ], maleFirstnames = [
    'Jiří',
    'Jan',
    'Petr',
    'Josef',
    'Jaroslav',
    'Pavel',
    'Martin',
    'Tomáš',
    'Miroslav',
    'Zdeněk',
    'Michal',
    'Milan',
    'Václav',
    'Jakub',
    'Lukáš',
    'František',
    'David',
    'Vladimír',
    'Karel',
    'Ondřej',
  ], femaleFirstnames = [
    'Jana',
    'Marie',
    'Hana',
    'Eva',
    'Věra',
    'Lenka',
    'Kateřina',
    'Lucie',
    'Anna',
    'Petra',
    'Alena',
    'Jaroslava',
    'Veronika',
    'Tereza',
    'Jitka',
    'Martina',
    'Michaela',
    'Helena',
    'Monika',
    'Ivana',
  ],
    workloads = [
      10,
      20,
      30,
      40,
    ];

  /**
   * @param {string} gender
   * @returns {string}
   */
  const getRandomSurname = function (gender) {
      const surnames = gender === FEMALE ? femaleSurnames : maleSurnames;
      return surnames[_getRandom(0, surnames.length - 1)];
    },
    /**
     * @param {string} gender
     * @returns {string}
     */
    getRandomFirstname = function (gender) {
      const surnames = gender === FEMALE ? femaleFirstnames : maleFirstnames;
      return surnames[_getRandom(0, surnames.length - 1)];
    },
    /**
     * @param {number} minOffset
     * @param {number} maxOffset
     * @returns {Date}
     */
    getRandomBirthdate = function (minOffset, maxOffset) {
      const currentDate = new Date(),
        minDate = (new Date((new Date()).setFullYear(currentDate.getFullYear() - maxOffset))),
        maxDate = (new Date((new Date()).setFullYear(currentDate.getFullYear() - minOffset)));
      return new Date(_getRandom(minDate.getTime(), maxDate.getTime()));
    },
    /**
     * @param {Date} date
     * @returns {string}
     */
    formatDate = function (date) {
      return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    },
    /**
     * @returns {number}
     */
    getRandomWorkload = function () {
      return workloads[_getRandom(0, workloads.length - 1)];
    };

  let dtoOut = [];

  for (let i = 0; i < dtoIn.count; i++) {
    const gender = _getRandom(0, 1) ? FEMALE : MALE;
    let employee = {
      name: getRandomFirstname(gender),
      surname: getRandomSurname(gender),
      gender: gender,
      birthdate: formatDate(getRandomBirthdate(dtoIn.age.min, dtoIn.age.max)),
      workload: getRandomWorkload(),
    };
    dtoOut.push(employee);

    console.info(_getCard(employee.name, employee.surname, employee.birthdate, employee.gender));
  }

  console.info(_getData(dtoOut));

  /*@@viewOff:sourceCode*/

  return { uuAppErrorMap };
}

main();
