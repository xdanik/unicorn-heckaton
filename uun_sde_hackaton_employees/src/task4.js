const AppClient = require("uu_appg01_server").AppClient;
const { Validator } = require("uu_appg01_server").Validation;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const { UseCaseError } = require("uu_appg01_server").AppServer;

const { dtoIn, console, session } = scriptContext;

/*@@viewOn:dtoIn*/
const dtoInSchema = `const sdeExamTask04DtoInSchemaType = array(
  shape({
    gender: oneOf("male","female").isRequired(),
    birthdate: date().isRequired(),
    name: string().isRequired(),
    surname: string().isRequired(),
    workload: oneOf(40,30,20,10).isRequired()
  }), 2000)`;
/*@@viewOff:dtoIn*/

/*@@viewOn:errors*/
const Errors = {
  ERROR_PREFIX: "ucl/sdeExamTask04/",

  InvalidDtoIn: class extends UseCaseError {
    constructor(dtoOut, paramMap) {
      super({ dtoOut, paramMap, status: 400 });
      this.message = `DtoIn is not valid.`;
      this.code = `${Errors.ERROR_PREFIX}invalidDtoIn`;
    }
  }
};
/*@@viewOff:errors*/

/*@@viewOn:helpers*/
async function _validateDtoIn(dtoIn, uuAppErrorMap) {
  //2.1.1
  let dtoInValidator = new Validator(dtoInSchema, true);
  //2.1.2
  let validationResult = dtoInValidator.validate("sdeExamTask04DtoInSchemaType", dtoIn);
  uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult, `${Errors.ERROR_PREFIX}unsupportedKeys`, Errors.InvalidDtoIn);
  //2.1.3
  return uuAppErrorMap;
}

function _getBarChart(headerText, valueLabel, data) {
  return `<UU5.Bricks.Section header="${headerText}">
    <UU5.SimpleChart.BarChart
      displayLegend
      series='<uu5json/>[
        {
          "valueKey": "value",
          "name": "${valueLabel}",
          "colorSchema": "blue-rich"
        }
      ]'>
      ${JSON.stringify(data)}
    </UU5.SimpleChart.BarChart>
  </UU5.Bricks.Section>`;
}
/*@@viewOff:helpers*/

/*@@viewOn:customHelpers*/

/*@@viewOff:customHelpers*/

async function main() {
  //1
  let uuAppErrorMap = {};

  //2
  await _validateDtoIn(dtoIn, uuAppErrorMap);

  /*@@viewOn:sourceCode*/

  const buildChart = function (chartLabel, employees) {
    let firstnameCounts = {};
    for (const employee of employees) {
      if (firstnameCounts.hasOwnProperty(employee.name)) {
        firstnameCounts[employee.name]++;
      } else {
        firstnameCounts[employee.name] = 1;
      }
    }
    let chartData = [];
    for (const [firstname, count] of Object.entries(firstnameCounts)) {
      chartData.push({
        label: firstname,
        value: count,
      });
    }
    return _getBarChart(chartLabel, "Zaměstnanci", chartData);
  }

  // task 4.1
  console.info(buildChart("všech zaměstnanců", dtoIn));

  // task 4.2
  console.info(buildChart("ženy", dtoIn.filter(employee => employee.gender === 'female')));

  // task 4.3
  console.info(buildChart("muži", dtoIn.filter(employee => employee.gender === 'male')));

  // task 4.4
  console.info(buildChart("ženy na zkrácený úvazek", dtoIn.filter(employee => employee.gender === 'female' && employee.workload !== 40)));

  // task 4.5
  console.info(buildChart("muži na plný pracovní úvazek", dtoIn.filter(employee => employee.gender === 'male' && employee.workload === 40)));

  /*@@viewOff:sourceCode*/

  return { uuAppErrorMap };
}

main();
