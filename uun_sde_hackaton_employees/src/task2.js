const AppClient = require("uu_appg01_server").AppClient;
const { Validator } = require("uu_appg01_server").Validation;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const { UseCaseError } = require("uu_appg01_server").AppServer;

const { dtoIn, console, session } = scriptContext;

/*@@viewOn:dtoIn*/
const dtoInSchema = `const sdeExamTask02DtoInSchemaType = array(
  shape({
    gender: oneOf("male","female").isRequired(),
    birthdate: date().isRequired(),
    name: string().isRequired(),
    surname: string().isRequired(),
    workload: oneOf(40,30,20,10).isRequired()
  }), 2000)`;
/*@@viewOff:dtoIn*/

/*@@viewOn:errors*/
const Errors = {
  ERROR_PREFIX: "ucl/sdeExamTask02/",

  InvalidDtoIn: class extends UseCaseError {
    constructor(dtoOut, paramMap) {
      super({ dtoOut, paramMap, status: 400 });
      this.message = `DtoIn is not valid.`;
      this.code = `${Errors.ERROR_PREFIX}invalidDtoIn`;
    }
  }
};
/*@@viewOff:errors*/

/*@@viewOn:helpers*/
async function _validateDtoIn(dtoIn, uuAppErrorMap) {
  //2.1.1
  let dtoInValidator = new Validator(dtoInSchema, true);
  //2.1.2
  let validationResult = dtoInValidator.validate("sdeExamTask02DtoInSchemaType", dtoIn);
  uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult, `${Errors.ERROR_PREFIX}unsupportedKeys`, Errors.InvalidDtoIn);
  //2.1.3
  return uuAppErrorMap;
}
/*@@viewOff:helpers*/

/*@@viewOn:customHelpers*/
/*@@viewOff:customHelpers*/

async function main() {
  //1
  let uuAppErrorMap = {};

  //2
  await _validateDtoIn(dtoIn, uuAppErrorMap);

  /*@@viewOn:sourceCode*/

  /**
   * @param {Number} value
   * @param {Number} decimals
   * @returns {string}
   */
  const formatNumber = function (value, decimals = 2) {
    return (Math.round(value * 10 ** decimals) / 10 ** decimals).toFixed(decimals);
  }

  // task 2.1
  console.info('počet zaměstnanců: ' + dtoIn.length);

  // task 2.2
  let employeesWorkloadCount = {
    '10': 0,
    '20': 0,
    '30': 0,
    '40': 0,
  }
  for (const employee of dtoIn) {
    employeesWorkloadCount[employee.workload.toString(10)]++;
  }
  console.info('počet zaměstnanců podle výše úvazku: ' + JSON.stringify(employeesWorkloadCount));

  // task 2.3
  /**
   * @param {int[]} array
   */
  const average = function (array) {
    let total = 0;
    for (const value of array) {
      total += value;
    }
    return total / array.length;
  },
    getBirthdayYear = function (employee) {
      const birthdate = new Date(employee.birthdate);
      return birthdate.getFullYear();
    },
    avgBirthdayYear = average(dtoIn.map(getBirthdayYear)),
    avgAge = (new Date()).getFullYear() - avgBirthdayYear;

  console.info('průměrný věk: ' + formatNumber(avgAge, 1));

  // task 2.4
  const getTimestamp = function (employee) {
    const birthdate = new Date(Date.parse(employee.birthdate));
    return birthdate.getTime();
  };

  let minAgeTimestamp = Math.min(...dtoIn.map(employee => getTimestamp(employee)));
  console.info('minimální věk: ' + (new Date(minAgeTimestamp)).toISOString());

  // task 2.5
  let maxAgeTimestamp = Math.max(...dtoIn.map(employee => getTimestamp(employee)));
  console.info('maximální věk: ' + (new Date(maxAgeTimestamp)).toISOString());

  // task 2.6
  const median = function (array) {
    if (array.length === 0) {
      return 0;
    }

    array.sort(function (a, b) {
      return a - b;
    });

    const half = Math.floor(array.length / 2);

    if (array.length % 2) {
      return array[half];
    }
    return (array[half - 1] + array[half]) / 2.0;
  },
    medianOfAge = median(dtoIn.map(employee => (new Date()).getFullYear() - getBirthdayYear(employee)));
  console.info('medián věku: ' + medianOfAge);

  // task 2.7
  const sortedEmployeesByWorkload = dtoIn.map(value => value).sort(function (a, b) {
    return a.workload - b.workload;
  });
  console.info('přehled zaměstnanců setříděných dle výše úvazků od nejmenšího po největší: ' + JSON.stringify(sortedEmployeesByWorkload));

  // task 2.7
  const employeesWorkloads = dtoIn.map(employee => employee.workload);
  console.info('medián výše úvazku: ' + median(employeesWorkloads));

  // task 2.8
  const femaleWorkloads = dtoIn.filter(employee => employee.gender === 'female').map(employee => employee.workload);
  console.info('průměrná výše úvazku v rámci žen: ' + formatNumber(average(femaleWorkloads), 2));

  /*@@viewOff:sourceCode*/

  return { uuAppErrorMap };
}

main();
